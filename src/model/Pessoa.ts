import { PlanoSaude } from './PlanoSaude';

export class Pessoa {
    
    id: number;
    nome: string;
    dataNascimento: string;
    email: string;
    planoSaude= new PlanoSaude;
    nomePlanoSaude: string;
  }