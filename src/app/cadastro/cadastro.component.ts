import { Pessoa } from './../../model/Pessoa';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ServicoPessoaService } from '../services/servico-pessoa.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
 
  pessoa: Pessoa = new Pessoa();
  mensagem: string;
  cadastroForm = new FormGroup({
    nome: new FormControl(),
    dataNascimento: new FormControl(),
    email: new FormControl(),
    planoSaude: new FormControl()
  });

  constructor(private pessoaService: ServicoPessoaService) { }

  ngOnInit() {
  }

  salvarBeneficiario(pessoa: Pessoa){
    this.pessoaService.salvarPessoa(pessoa);
  }

  get f() { return this.cadastroForm.controls; }

  cadastrar() {
    this.pessoa.nome= this.f.nome.value;
    this.pessoa.dataNascimento= this.f.dataNascimento.value;
    this.pessoa.email= this.f.email.value;
    this.pessoa.planoSaude.idPlano= this.f.planoSaude.value;
    
    this.pessoaService.salvarPessoa(this.pessoa).then((resposta: any) => {
      console.log(resposta);
      this.cadastroForm.reset();
      alert("Cadastrado com sucesso");
    });
  }

}
