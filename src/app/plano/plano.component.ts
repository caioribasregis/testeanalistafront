import { Component, OnInit } from '@angular/core';
import { ProcedimentoService } from '../services/procedimento.service';
import { ServicoPessoaService } from '../services/servico-pessoa.service';
import { Pessoa } from 'src/model/Pessoa';
import { Procedimento } from 'src/model/Procedimento';
import { VerificacaoPlanoService } from '../services/verificacao-plano.service';

@Component({
  selector: 'app-plano',
  templateUrl: './plano.component.html',
  styleUrls: ['./plano.component.css']
})
export class PlanoComponent implements OnInit {

 listaPessoas: Array<any>;
 listaProcedimentos: Array<any>;
 idPessoa: number;
 idProcedimento: number;
 
  constructor(private pessoaService: ServicoPessoaService, private procedimentoService: ProcedimentoService, private verificacaoService: VerificacaoPlanoService) { }

  ngOnInit() {
    this.listarPessoa();
    this.listarProcedimento();
  }

  listarPessoa(){
    this.pessoaService.listarPessoa().subscribe(resp => this.listaPessoas = resp )
  }

  listarProcedimento(){
    this.procedimentoService.listarProcedimento().subscribe(resposta => this.listaProcedimentos = resposta )
  }

  verificarProcedimento(){
    debugger;
    this.verificacaoService.verificaProcedimento(this.idPessoa, this.idProcedimento).subscribe(res => {
      debugger;
      alert(res['mensagem']);
    }, err => {
      console.error(err);
      alert(err);
    });   
  }

  variavelIdPessoa(event: number){
    this.idPessoa= event;
  }

  variavelIdProcedimento(event: number){
    this.idProcedimento= event;
  }

}
