import { HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaBeneficiariosComponent } from './lista-beneficiarios/lista-beneficiarios.component';
import { PlanoComponent } from './plano/plano.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { EditarComponent } from './editar/editar.component';
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms';
import { ServicoPessoaService } from './services/servico-pessoa.service';
import { ListaPessoaService } from './services/lista-pessoa.service';


@NgModule({
  declarations: [
    AppComponent,
    ListaBeneficiariosComponent,
    PlanoComponent,
    CadastroComponent,
    EditarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ ListaPessoaService, ServicoPessoaService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
