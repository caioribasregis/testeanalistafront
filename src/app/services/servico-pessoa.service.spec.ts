import { TestBed } from '@angular/core/testing';

import { ServicoPessoaService } from './servico-pessoa.service';

describe('ServicoPessoaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServicoPessoaService = TestBed.get(ServicoPessoaService);
    expect(service).toBeTruthy();
  });
});
