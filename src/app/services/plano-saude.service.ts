import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PlanoSaude } from 'src/model/PlanoSaude';

@Injectable({
  providedIn: 'root'
})
export class PlanoSaudeService {

  urlAPI = 'http://localhost:8080/teste-web/rest/planoSaude';

  constructor( private http: HttpClient ) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  listarPlanoSaude(){
    return this.http.get<PlanoSaude[]>(this.urlAPI + '/listar');
  }


}
