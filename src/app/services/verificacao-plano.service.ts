import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pessoa } from 'src/model/Pessoa';
import { Procedimento } from 'src/model/Procedimento';
import { stringify } from 'querystring';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VerificacaoPlanoService {

readonly urlAPI = 'http://localhost:8080/teste-web/rest/pessoa';

  constructor(private http: HttpClient ) { }

  verificaProcedimento(pessoa: number, procedimento: number): Observable<any> {
    
    return this.http.get<any>(this.urlAPI + `/verificarProcedimento/${pessoa}/${procedimento}`);

  }


}
