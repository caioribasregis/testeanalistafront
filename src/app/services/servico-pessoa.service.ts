import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pessoa } from 'src/model/Pessoa';

@Injectable({
  providedIn: 'root'
})
export class ServicoPessoaService {

urlAPI = 'http://localhost:8080/teste-web/rest/pessoa';

  constructor(private http: HttpClient ) {
    
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  listarPessoa(){
    return this.http.get<Pessoa[]>(this.urlAPI + '/listar');
  }

  salvarPessoa(pessoa: Pessoa): Promise<any>{
    return this.http.post(this.urlAPI + '/salvar', pessoa, this.httpOptions).toPromise();
  }

  excluirPessoa(pessoa: Pessoa): Promise<any>{
    return this.http.post(this.urlAPI + '/excluir', pessoa, this.httpOptions).toPromise();
  }

 


}