
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Procedimento } from 'src/model/Procedimento';
  
  @Injectable({
    providedIn: 'root'
  })
  export class ProcedimentoService {
  
  urlAPI = 'http://localhost:8080/teste-web/rest/procedimento';
  
    constructor(private http: HttpClient ) {  }
  
    httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  
    listarProcedimento(){
      return this.http.get<Procedimento[]>(this.urlAPI + '/listar');
    }
  
    salvarProcedimento(procedimento: Procedimento): Promise<any>{
      return this.http.post(this.urlAPI + '/salvar', procedimento, this.httpOptions).toPromise();
    }
  
  }