import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListaPessoaService {

  listaPessoaURL = 'http://localhost:8080/teste-web/rest/pessoa/listar';

  constructor(private http: HttpClient ) {
    
  }

  listarPessoa(){
    return this.http.get<any[]>(`${this.listaPessoaURL}`);
  }

}
