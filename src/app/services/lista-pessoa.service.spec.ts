import { TestBed } from '@angular/core/testing';

import { ListaPessoaService } from './lista-pessoa.service';

describe('ListaPessoaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListaPessoaService = TestBed.get(ListaPessoaService);
    expect(service).toBeTruthy();
  });
});
