import { TestBed } from '@angular/core/testing';

import { VerificacaoPlanoService } from './verificacao-plano.service';

describe('VerificacaoPlanoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VerificacaoPlanoService = TestBed.get(VerificacaoPlanoService);
    expect(service).toBeTruthy();
  });
});
