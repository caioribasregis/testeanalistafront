import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaBeneficiariosComponent } from './lista-beneficiarios/lista-beneficiarios.component';
import { PlanoComponent } from './plano/plano.component';
import { CadastroComponent } from './cadastro/cadastro.component';


const routes: Routes = [
  { path: 'lista-beneficiarios', component: ListaBeneficiariosComponent },
  { path: 'plano', component: PlanoComponent },
  { path: 'cadastro', component: CadastroComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
