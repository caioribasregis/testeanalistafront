import { Component, OnInit } from '@angular/core';
import { ServicoPessoaService } from '../services/servico-pessoa.service';
import { Pessoa } from 'src/model/Pessoa';


@Component({
  selector: 'app-lista-beneficiarios',
  templateUrl: './lista-beneficiarios.component.html',
  styleUrls: ['./lista-beneficiarios.component.css']
})
export class ListaBeneficiariosComponent implements OnInit {

  listaBeneficiarios: Array<Pessoa>;

  constructor(private pessoaService: ServicoPessoaService) { }

  ngOnInit() {
    this.listarPessoa();
  }

  listarPessoa(){
    this.pessoaService.listarPessoa().subscribe(resp => this.listaBeneficiarios = resp )
  }

  excluir(pessoa: Pessoa){
    this.pessoaService.excluirPessoa(pessoa).then((resposta: any) => {
      console.log(resposta);
      alert("Excluido");
    });
  }

}
